import pandas as pd
import requests
from bs4 import BeautifulSoup
import urllib.parse
from difflib import get_close_matches
import logging
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')

# Load the CSV file
file_path = '/Users/alexkumar/Downloads/fin_producers.csv'
data = pd.read_csv(file_path)

# Function to properly encode special characters in the producer names
def encode_special_characters(name):
    return urllib.parse.quote(name)

# Function to search Muso.ai for a producer and get their profile URL
def search_muso_for_profile(producer_name):
    try:
        encoded_name = encode_special_characters(producer_name)
        search_url = f'https://credits.muso.ai/search/profiles?keyword={encoded_name}'
        
        # Use Selenium to open the search URL and wait for results to load
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        driver = webdriver.Chrome(options=options)
        driver.get(search_url)

        # Wait for the search results to load
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//a[contains(@href, "profile")]'))
        )

        soup = BeautifulSoup(driver.page_source, 'html.parser')
        driver.quit()
        
        # Extract all profile links and names from the search results
        profiles = []
        for a in soup.find_all('a', href=True):
            if 'profile' in a['href']:
                profile_name = a.get_text(strip=True)
                profile_link = f"https://credits.muso.ai{a['href']}"
                profiles.append((profile_name, profile_link))

        # Check if the top result matches the producer name exactly (case insensitive)
        if profiles and profiles[0][0].lower() == producer_name.lower():
            return profiles[0][1]

        # Check for any perfect match from the rest of the search results (case insensitive)
        for profile in profiles:
            if profile[0].lower() == producer_name.lower():
                return profile[1]

        # If no exact match, find the closest match to the producer name
        profile_names = [profile[0] for profile in profiles]
        closest_match = get_close_matches(producer_name, profile_names, n=1, cutoff=0.6)
        if closest_match:
            for profile in profiles:
                if profile[0] == closest_match[0]:
                    return profile[1]
        return None
    except Exception as e:
        logging.error(f"Error searching for profile: {e}")
        return None

# Function to extract Instagram link from a profile page
def extract_instagram_link(profile_url):
    try:
        response = requests.get(profile_url)
        if response.status_code == 200:
            soup = BeautifulSoup(response.content, 'html.parser')
            # Find the Instagram link in the profile page
            instagram_link = None
            for a in soup.find_all('a', href=True):
                if 'instagram.com' in a['href']:
                    instagram_link = a['href']
                    break
            return instagram_link
        else:
            return None
    except Exception as e:
        logging.error(f"Error extracting Instagram link: {e}")
        return None

# Initialize empty lists to store the results
profile_urls = []
instagram_links = []

# Search for each producer and get their profile URL and Instagram link
for producer_name in data['Producer name']:
    logging.info(f"Searching for producer: {producer_name}")
    profile_url = search_muso_for_profile(producer_name)
    profile_urls.append(profile_url)
    instagram_link = extract_instagram_link(profile_url) if profile_url else None
    instagram_links.append(instagram_link)

# Add the results to the DataFrame
data['Producer Profile Link'] = profile_urls
data['Instagram Link'] = instagram_links

# Save the results to a new CSV file
output_file_path = '/Users/alexkumar/Downloads/MusoScraper/Results/producers-FIN.csv'
data.to_csv(output_file_path, index=False)

logging.info(f'Instagram links have been extracted and saved to {output_file_path}')
