import pandas as pd
import requests
from bs4 import BeautifulSoup

# Load the CSV file
file_path = '/Users/alexkumar/Downloads/extract-data-from-charts-producers-fin_weekly-top-producers.csv'
data = pd.read_csv(file_path)

# Function to extract Instagram link from a profile page
def extract_instagram_link(profile_url):
    try:
        response = requests.get(profile_url)
        if response.status_code == 200:
            soup = BeautifulSoup(response.content, 'html.parser')
            # Find the Instagram link in the profile page
            instagram_link = None
            for a in soup.find_all('a', href=True):
                if 'instagram.com' in a['href']:
                    instagram_link = a['href']
                    break
            return instagram_link
        else:
            return None
    except Exception as e:
        return None

# Extract Instagram links for all URLs in the CSV
data['Instagram Link'] = data['Producer Profile Link'].apply(extract_instagram_link)

# Save the results to a new CSV file
output_file_path = '/Users/alexkumar/Downloads/producers_with_instagram_links.csv'
data.to_csv(output_file_path, index=False)

print(f'Instagram links have been extracted and saved to {output_file_path}')
