import requests
from bs4 import BeautifulSoup
import pandas as pd

# URL of the Billboard Hot 100 Producers chart
url = 'https://www.billboard.com/charts/hot-100-producers/'

# Send a request to the URL
response = requests.get(url)

# Check if the request was successful
if response.status_code == 200:
    soup = BeautifulSoup(response.content, 'html.parser')
    
    # Find the specific section containing the producer names
    chart_section = soup.select_one('#post-1235074055 > div.pmc-paywall > div > div > div > div.chart-results-list')
    
    # Extract producer names from the section
    producers = []
    if chart_section:
        producer_elements = chart_section.find_all('h3', class_='c-title')
        unwanted_texts = ["Gains in Weekly Performance", "Additional Awards", "Imprint/Promotion Label:"]
        for producer in producer_elements:
            text = producer.get_text(strip=True)
            if text and text not in unwanted_texts:
                producers.append(text)
    
    # Save the producer names to a DataFrame
    df = pd.DataFrame(producers, columns=['Producer name'])
    
    # Save the DataFrame to a CSV file
    output_file_path = '/Users/alexkumar/Downloads/MusoScraper/billboard_hot_100_producers.csv'
    df.to_csv(output_file_path, index=False)
    
    print(f'Producer names have been scraped and saved to {output_file_path}')
else:
    print('Failed to retrieve the webpage.')
