#!/usr/bin/env python3
#
# spotify-song-credits
# Scrape song credits from Spotify's web player
#

import csv
import os
import logging
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import time

# Logging configuration
log_path = '/tmp/geckobrowser.log'
log_level = logging.INFO

def accept_cookies(browser):
    try:
        logging.info("Checking if cookies are accepted")
        cookie_banner = WebDriverWait(browser, 1).until(
            EC.visibility_of_element_located((By.ID, 'onetrust-banner-sdk'))
        )
        accept_button = cookie_banner.find_element(By.XPATH, '//*[@id="onetrust-accept-btn-handler"]')
        if accept_button.is_displayed():
            accept_button.click()
            logging.info("Cookies accepted")
        else:
            logging.info("Cookies already accepted")
    except TimeoutException:
        logging.info("No cookie banner found, skipping cookie acceptance")
    except Exception as e:
        logging.error(f"An error occurred while accepting cookies: {e}")

def spotify_song_credits(browser, url_payload, csv_writer):
    try:
        logging.info(f"Loading the Spotify track page: {url_payload}")
        browser.get(url_payload)
        accept_cookies(browser)
        time.sleep(3)  # Adjust if needed to wait for the page to load completely
        
        # Fetching song name
        song_name = WebDriverWait(browser, 10).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="main"]/div/div[2]/div[3]/div[1]/div[2]/div[2]/div[2]/main/section/div[1]/div[3]/div[3]/span[2]/h1'))
        ).text
        
        # Showing song credits
        spotify_show_song_options(browser)
        time.sleep(1)
        spotify_show_credits(browser)
        time.sleep(1)
        
        # Extracting song credits
        credits = {}
        modal = WebDriverWait(browser, 10).until(
            EC.visibility_of_element_located((By.XPATH, '/html/body/div[21]/div/div'))
        )
        
        # Helper function to extract credits
        def extract_credits(label):
            elements = modal.find_elements(By.XPATH, f".//div[contains(p, '{label}')]/span")
            return '; '.join([element.text for element in elements])
        
        credits['performed_by'] = extract_credits('Performed by')
        credits['written_by'] = extract_credits('Written by')
        credits['produced_by'] = extract_credits('Produced by')

        # Writing to CSV
        csv_writer.writerow([song_name, credits['performed_by'], credits['written_by'], credits['produced_by']])
        
        logging.info(f"Song credits extracted for {song_name}: Performed by: {credits['performed_by']}, Written by: {credits['written_by']}, Produced by: {credits['produced_by']}")
    except Exception as e:
        logging.error(f"An error occurred while getting song credits for {url_payload}: {e}")

def spotify_show_song_options(browser):
    try:
        logging.info("Showing song options")
        WebDriverWait(browser, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="main"]/div/div[2]/div[3]/div[1]/div[2]/div[2]/div[2]/main/section/div[3]/div[2]/div/div/button[2]'))
        ).click()
    except Exception as e:
        logging.error(f"An error occurred while showing song options: {e}")

def spotify_show_credits(browser):
    try:
        logging.info("Showing song credits")
        WebDriverWait(browser, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="context-menu"]/ul/li[6]/button'))
        ).click()
    except Exception as e:
        logging.error(f"An error occurred while showing credits: {e}")

if __name__ == '__main__':
    logging.basicConfig(level=log_level)

    # Define the path to the input CSV file containing track URLs
    input_csv_path = '/Users/alexkumar/Downloads/playlist_urls_Top_50_SE.csv'  # Replace with your actual CSV file path

    # Read URLs from the specified CSV file
    urls = []
    with open(input_csv_path, newline='') as csvfile:
        reader = csv.reader(csvfile)
        next(reader, None)  # Skip the header row
        for row in reader:
            urls.append(row[0])

    # Path to the WebDriver executable
    webdriver_path = '/Users/alexkumar/Downloads/geckodriver'  # Replace with your actual path
    download_dir = '/Users/alexkumar/Downloads/MusoScraper/SpotifyScrapes/'

    # Ensure the download directory exists
    os.makedirs(download_dir, exist_ok=True)

    # Initialize the WebDriver
    options = Options()
    options.headless = False  # Set to True if you want to run in headless mode
    options.set_preference("browser.download.folderList", 2)
    options.set_preference("browser.download.dir", download_dir)
    options.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/csv")

    service = Service(executable_path=webdriver_path)
    browser = webdriver.Firefox(service=service, options=options)

    try:
        with open(os.path.join(download_dir, 'song_credits_100_SE.csv'), 'w', newline='', encoding='utf-8') as csvfile:
            csv_writer = csv.writer(csvfile)
            csv_writer.writerow(['Songname', 'Performed by', 'Written by', 'Produced by'])  # Write header
            
            for url in urls:
                spotify_song_credits(browser, url, csv_writer)
    finally:
        browser.quit()
